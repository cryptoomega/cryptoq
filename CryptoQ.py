
from exchanges.Gdax import *
from TradeAgent import *

import os, sys
import random
import time
import numpy as np
from datetime import datetime
from shutil import copyfile

EPISODES = 100;
BATCH_SIZE = 128;

logger = logging.getLogger('GDAX CryptoQ.')


def pretrain(exchange, trade_agent):
    STATE_SIZE = exchange.state_size;

    start = date(2016, 6, 1);
    start = datetime.combine(start, datetime.min.time())
    end   = date(2018, 2, 12);
    end   = datetime.combine(end, datetime.min.time())
    # exchange.download_historic_data(exchange.market, start, end, 300);          # Download data the first time you run this program.
    # return;
    exchange.turn_simulation('ON');
    # trade_agent.load_model("7.0")
    # trade_agent.epsilon = 0.001;

    EPISODES = len(exchange.training_data_paths)
    #EPISODES = 50
    profit_records = open("profit_records.txt", 'w')


    for e in range(0,EPISODES*10):
        total_reward = 0.0;
        if e == len(exchange.training_data_paths):
            exchange.logger.info("TRAINING FINISHED.")
            return;

        state = exchange.reset(e%EPISODES)
        exchange.logger.info(exchange.training_data_paths[e])
        state, _, _, _ = exchange.take_action(1 if random.random()>0.5 else 0, "market")
        state = np.reshape(state, [1, STATE_SIZE])
        for time in range(1000000):
            # 1. predict action based on the state
            action = trade_agent.predict(state)

            # 2. take action
            next_state, reward, done, _ = exchange.take_action(action, "market") # Turn the last parameter to False to turn off trade print

            total_reward += reward
            #print(total_reward)
            next_state = np.reshape(next_state, [1, STATE_SIZE])

            # 3. record historic data of training
            trade_agent.remember(state, action, reward, next_state, done)

            # 4. train the agent
            if len(trade_agent.memory) > BATCH_SIZE:
                trade_agent.replay(BATCH_SIZE)

            # 5. prepare for the next prediction;
            state = next_state

            if done:
                buy_hold_profit = exchange.init_total_usd * (exchange.sim_data[-1][4]-exchange.sim_data[30][4])/exchange.sim_data[30][4]
                logger.info("Episode: {0}/{1}, score: {2:05.5f}, Total: {3:.2f} | Buy-Hold profit: {4:.2f}"\
                             .format(e, EPISODES, total_reward, exchange._get_total_worth(), buy_hold_profit))

                agent_profit_rate = (exchange._get_total_worth()-exchange.init_total_usd)/exchange.init_total_usd;
                buy_hold_profit_rate = (exchange.sim_data[-1][4]-exchange.sim_data[30][4])/exchange.sim_data[30][4]
                profit_records.write( "{0}\t{1}\t{2}\n".format(e,agent_profit_rate, buy_hold_profit_rate) )
                profit_records.flush()
                # copy log file
                '''
                exchange.flush_log();
                if total_reward >= 1000:
                    logpath = os.path.abspath("./logs/")
                    log_file = "Gdax.log"
                    dst_filename = exchange.training_data_paths[e].split("/")[-1]
                    dst_filename = dst_filename.split(".")[0] + ".log"
                    src = os.path.join(logpath, log_file)
                    dst = os.path.join(logpath, dst_filename)
                    copyfile(src, dst)
                log_file = open("./logs/Gdax.log", 'w');
                log_file.truncate()
                log_file.flush();
                log_file.close()
                '''
                break
        if e > 0 and e%50 == 0:
            trade_agent.save_model(str(e/50))

    trade_agent.save_model("Final")
    pass;

def backtesting(exchange, trade_agent):
    STATE_SIZE = exchange.state_size;

    exchange.turn_simulation('ON');
    trade_agent.load_model("./saved_models/good_model4.h5")
    trade_agent.epsilon = 0.0001;

    EPISODES = len(exchange.training_data_paths)

    for e in range(0,EPISODES):
        total_reward = 0.0;

        state = exchange.reset(e)
        print(exchange.training_data_paths[e])
        state, _, _, _ = exchange.take_action(1 if random.random()>0.5 else 0, "market")
        state = np.reshape(state, [1, STATE_SIZE])
        for time in range(1000000):
            # 1. predict action based on the state
            action = trade_agent.predict(state)

            # 2. take action
            next_state, reward, done, _ = exchange.take_action(action, "market") # Turn the last parameter to False to turn off trade print

            total_reward += reward
            #print(total_reward)
            next_state = np.reshape(next_state, [1, STATE_SIZE])

            # 3. record historic data of training
            trade_agent.remember(state, action, reward, next_state, done)

            # 5. prepare for the next prediction;
            state = next_state

            if done:
                buy_hold_profit = exchange.init_total_usd * (exchange.sim_data[-1][-1]-exchange.sim_data[0][-1])/exchange.sim_data[0][-1]
                logger.info("Episode: {0}/{1}, score: {2:05.5f}, Total: {3:.2f} | Buy-Hold profit: {4:.2f}"\
                             .format(e, EPISODES, total_reward, exchange._get_total_worth(), buy_hold_profit))
                with open('./logs/Gdax.log', 'w'):
                    pass
                break
    pass;

def trade(exchange, trade_agent):

    # load model
    trade_agent.load_model("./saved_models/good_model4.h5");
    trade_agent.epsilon = -0.0001;
    exchange.turn_simulation('OFF');
    exchange.reset(0)
    STATE_SIZE = exchange.state_size;

    while True:
        # This while loop waits for some minutes
        while True:
            time.sleep(0.8);
            now = datetime.now();
            if int(now.minute % (exchange.granularity/60)) == 0:
                break;
        try:
            state = exchange.get_state();
            state = np.reshape(state, [1, STATE_SIZE])
            action = trade_agent.predict(state)
            exchange.take_action(action, "limit")
        except Exception as e:
            print(e)
            continue;

        # This while loop prevents multiple trading in the same minute
        while True:
            time.sleep(0.8);
            now = datetime.now();
            if now.minute % (exchange.granularity/60) >= 1:
                break;

    pass;

if __name__ == "__main__":
    exchange = GDAX();
    agent    = TradeAgent(exchange.state_size, [128, 64, 64, 64, 32 ]) # [64, 128, 256, 128, 64] was great
    pretrain(exchange, agent);
    #backtesting(exchange, agent)
    #trade(exchange, agent);
