# -*- coding: utf-8 -*-
# Reference:
# 1. https://github.com/keon/deep-q-learning/blob/master/ddqn.py

import random
import numpy as np
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam, RMSprop, SGD
from keras import backend as K
import json
from keras.models import load_model

# Deep Q-learning Agent
class TradeAgent:
    def __init__(self, state_size, network_sizes):
        '''@param state_size: an integer indicating the dimensionality of input data
        @param network_sizes: a list containing the sizes of each layer
        '''
        self.state_size = state_size
        self.action_size = 2;  # Buy, or Sell (Sell when there is nothing to sell is equivalent to HOLD)
        self.memory = deque(maxlen=4000)
        self.gamma = 0.95    # discount rate
        self.epsilon = 0.999   # exploration rate
        self.epsilon_min = 0.05
        self.epsilon_decay = 0.999
        self.learning_rate = 0.0001
        self.model = self._build_model(network_sizes)

    def _build_model(self, network_sizes):
        # Neural Net for Deep-Q learning Model
        if network_sizes is None or len(network_sizes) < 1:
            raise Exception("Invalid network structure")
        model = Sequential()

        # build input layer
        model.add(Dense(network_sizes[0], input_dim=self.state_size, activation='relu'))

        # build hidden layers.
        if len(network_sizes) > 1:
            for size in network_sizes[1:]:
                model.add(Dense(size, activation='relu'))

        #  build output layer
        model.add(Dense(self.action_size, activation='linear'))
        model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))

        return model

    def save_model(self, name):
        self.model.save('./saved_models/agent_model_{}.h5'.format(name))

    def load_model(self, name):
        self.model = load_model(name)

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def predict(self, state):
        '''Predict which action to take'''
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])  # returns action

    def replay(self, batch_size):
        minibatch = random.sample(self.memory, batch_size)
        for state, action, reward, next_state, done in minibatch:
            target = reward
            if not done:
              target = reward + self.gamma * \
                       np.amax(self.model.predict(next_state)[0])
            target_f = self.model.predict(state)
            target_f[0][action] = target
            self.model.fit(state, target_f, epochs=1, verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay






# SOme good parameters:
#-----------------------------------------------
# networksizes = [64, 128, 128, 128, 128, 64]
# self.gamma = 0.95    # discount rate
# self.epsilon = 0.95   # exploration rate
# self.epsilon_min = 0.005
# self.epsilon_decay = 0.99
# self.learning_rate = 0.00001
#-----------------------------------------------
# networksizes = [64, 128, 256, 128, 64]
# self.gamma = 0.95    # discount rate
# self.epsilon = 0.95   # exploration rate
# self.epsilon_min = 0.005
# self.epsilon_decay = 0.99
# self.learning_rate = 0.0001
