# CryptoQ
**CryptoQ** is a program that applies Deep Q-learning to crypto trading. This project is still under development, and no actual trading is performed. The simulation is also not guaranteed to be correct. We are still debugging the simulation to make sure the result is from our actor network, instead of bugs.  

Please contact [ian.zhang1990@gmail.com](mailto:ian.zhang1990@gmail.com) if you find any bugs.

# Please do not distribute the code. For our own interests, I hope this rule is strict, and the code is shared only within our small group.


## Preliminaries
Following is a list of required libraries. Please install them before running this program.

#### 1. Tensorflow
`pip3 install tensorflow`

For more information on installation, please read [https://www.tensorflow.org/install/](https://www.tensorflow.org/install/). No GPU installation is needed.

#### 2. Keras for easier neural network
`pip3 install keras`

For more info, please read [https://keras.io/](https://keras.io/)

You are also going to need `h5py` to save & load trained models.

`pip3 install h5py`


#### 3. GDAX python API.
`pip3 install gdax`

For more info, please read [https://github.com/danpaquin/gdax-python](https://github.com/danpaquin/gdax-python)

## Tuning the network.
As deep neural networks are subject to heavy manual turning works, our program will surely suffer the same issue. It's not verified if the same code runs the same results on every machine. Most likely, you will have to tune the parameters on your own machine.

Following are a list of parameters you can change. Some are marked with bold font, meaning that you have to re-download the training data.

* Network sizes, at main() function in CryptoQ.py.
* Learning rate, in TradeAgent.py. Usually start from 1e-4  to 1e-7.
* epsilon_decay, in TradeAgent.py. Usually start from 0.9 to 1.0.
* **PERIODS**  , in config file. This indicates how many periods of price changes you want to feed the neural network as a current state.
* **GRANULARITY**, in config file. Applied as the last parameter of download_historic_data() in Gdax.py


### Some simulation results
Following are some simulation results using market data from 02-01-2017 to 02-06-2018. During these days, we observed huge soar and pops of BTC price. Our neural network is able to get consistent results.

Every day is a new episode. The balance is set to have 1000 USD at the beginning of each day. Here, the "Total" column is the close balance at the end of the day. Buy-Hold is a comparison strategy that buys BTC at the beginning and sell at the end.  

```
./training_data/2018-01-01.data
2018-02-12 11:41:29,310 | INFO | Episode: 331/371, score: 857.85187, Total: 1088.80 | Buy-Hold profit: -34.97
./training_data/2018-01-02.data
2018-02-12 11:42:51,103 | INFO | Episode: 332/371, score: 159.99825, Total: 1016.00 | Buy-Hold profit: 98.45
./training_data/2018-01-03.data
2018-02-12 11:44:13,198 | INFO | Episode: 333/371, score: 110.15485, Total: 1011.62 | Buy-Hold profit: 19.54
./training_data/2018-01-04.data
2018-02-12 11:45:34,999 | INFO | Episode: 334/371, score: 497.36565, Total: 1049.74 | Buy-Hold profit: 2.80
./training_data/2018-01-05.data
2018-02-12 11:46:57,002 | INFO | Episode: 335/371, score: 1970.99274, Total: 1190.14 | Buy-Hold profit: 122.49
./training_data/2018-01-06.data
2018-02-12 11:48:18,661 | INFO | Episode: 336/371, score: 337.65031, Total: 1033.77 | Buy-Hold profit: 1.75
./training_data/2018-01-07.data
2018-02-12 11:49:40,684 | INFO | Episode: 337/371, score: 323.57956, Total: 1035.86 | Buy-Hold profit: -49.41
./training_data/2018-01-08.data
2018-02-12 11:51:02,421 | INFO | Episode: 338/371, score: 28.20524, Total: 1002.82 | Buy-Hold profit: -81.96
./training_data/2018-01-09.data
2018-02-12 11:52:24,427 | INFO | Episode: 339/371, score: 502.04382, Total: 1049.07 | Buy-Hold profit: -33.34
./training_data/2018-01-10.data
2018-02-12 11:53:46,142 | INFO | Episode: 340/371, score: 1052.84655, Total: 1103.75 | Buy-Hold profit: 9.02
./training_data/2018-01-11.data
2018-02-12 11:55:08,138 | INFO | Episode: 341/371, score: 1095.89114, Total: 1111.10 | Buy-Hold profit: -96.67
./training_data/2018-01-12.data
2018-02-12 11:56:29,906 | INFO | Episode: 342/371, score: 423.85226, Total: 1044.28 | Buy-Hold profit: 50.20
./training_data/2018-01-13.data
2018-02-12 11:57:51,978 | INFO | Episode: 343/371, score: 663.76866, Total: 1066.80 | Buy-Hold profit: 28.36
./training_data/2018-01-14.data
2018-02-12 11:59:13,715 | INFO | Episode: 344/371, score: 424.79198, Total: 1042.60 | Buy-Hold profit: -35.49
./training_data/2018-01-15.data
2018-02-12 12:00:35,812 | INFO | Episode: 345/371, score: 981.15437, Total: 1100.15 | Buy-Hold profit: -5.52
./training_data/2018-01-16.data
2018-02-12 12:01:57,475 | INFO | Episode: 346/371, score: 1229.89793, Total: 1122.88 | Buy-Hold profit: -148.39
./training_data/2018-01-17.data
2018-02-12 12:03:19,469 | INFO | Episode: 347/371, score: 1543.02163, Total: 1153.70 | Buy-Hold profit: -16.09
./training_data/2018-01-18.data
2018-02-12 12:04:41,050 | INFO | Episode: 348/371, score: 302.29586, Total: 1030.23 | Buy-Hold profit: -0.14
./training_data/2018-01-19.data
2018-02-12 12:06:02,970 | INFO | Episode: 349/371, score: -109.49498, Total: 989.05 | Buy-Hold profit: 13.06
./training_data/2018-01-20.data
2018-02-12 12:07:24,646 | INFO | Episode: 350/371, score: 1055.17705, Total: 1105.52 | Buy-Hold profit: 115.82
./training_data/2018-01-21.data
2018-02-12 12:08:47,526 | INFO | Episode: 351/371, score: 848.06402, Total: 1084.81 | Buy-Hold profit: -107.08
./training_data/2018-01-22.data
2018-02-12 12:10:10,036 | INFO | Episode: 352/371, score: 1422.89375, Total: 1142.29 | Buy-Hold profit: -48.06
./training_data/2018-01-23.data
2018-02-12 12:11:32,891 | INFO | Episode: 353/371, score: 1519.05845, Total: 1151.91 | Buy-Hold profit: 10.58
./training_data/2018-01-24.data
2018-02-12 12:12:55,457 | INFO | Episode: 354/371, score: 1142.94839, Total: 1111.98 | Buy-Hold profit: 27.91
./training_data/2018-01-25.data
2018-02-12 12:14:18,333 | INFO | Episode: 355/371, score: 183.69940, Total: 1014.01 | Buy-Hold profit: -7.14
./training_data/2018-01-26.data
2018-02-12 12:15:40,736 | INFO | Episode: 356/371, score: 1323.80934, Total: 1132.38 | Buy-Hold profit: 3.13
./training_data/2018-01-27.data
2018-02-12 12:17:02,920 | INFO | Episode: 357/371, score: 206.48036, Total: 1020.65 | Buy-Hold profit: 17.13
./training_data/2018-01-28.data
2018-02-12 12:18:24,796 | INFO | Episode: 358/371, score: 239.39010, Total: 1023.34 | Buy-Hold profit: 23.91
./training_data/2018-01-29.data
2018-02-12 12:19:46,862 | INFO | Episode: 359/371, score: 32.80596, Total: 1004.99 | Buy-Hold profit: -30.76
./training_data/2018-01-30.data
2018-02-12 12:21:08,643 | INFO | Episode: 360/371, score: -323.67079, Total: 965.39 | Buy-Hold profit: -104.52
./training_data/2018-01-31.data
2018-02-12 12:22:30,753 | INFO | Episode: 361/371, score: 1031.68217, Total: 1104.56 | Buy-Hold profit: 4.75
./training_data/2018-02-01.data
2018-02-12 12:23:52,535 | INFO | Episode: 362/371, score: 130.53137, Total: 1013.05 | Buy-Hold profit: -93.39
./training_data/2018-02-02.data
2018-02-12 12:25:14,583 | INFO | Episode: 363/371, score: 1143.73982, Total: 1114.37 | Buy-Hold profit: -36.51
./training_data/2018-02-03.data
2018-02-12 12:26:36,335 | INFO | Episode: 364/371, score: 1153.85710, Total: 1115.39 | Buy-Hold profit: 41.89
./training_data/2018-02-04.data
2018-02-12 12:27:58,330 | INFO | Episode: 365/371, score: 622.77097, Total: 1072.97 | Buy-Hold profit: -87.95
./training_data/2018-02-05.data
2018-02-12 12:29:19,975 | INFO | Episode: 366/371, score: 506.03994, Total: 1050.60 | Buy-Hold profit: -167.80
./training_data/2018-02-06.data
2018-02-12 12:30:41,922 | INFO | Episode: 367/371, score: 4629.86296, Total: 1462.83 | Buy-Hold profit: 138.26
./training_data/2018-02-07.data
2018-02-12 12:32:03,491 | INFO | Episode: 368/371, score: 1026.77848, Total: 1092.93 | Buy-Hold profit: -17.61
./training_data/2018-02-08.data
2018-02-12 12:33:25,440 | INFO | Episode: 369/371, score: 409.40553, Total: 1040.94 | Buy-Hold profit: 80.40

```
