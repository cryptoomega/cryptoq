from abc import ABC

class Exchange(ABC):
    def reset(self, episode):
        '''Reset parameters.
        @param episode: integer number indicating which episode we are running now.
        @return: initial state/observation.'''
        pass;

    def read_config(self, path):
        '''Read configuration file'''
        pass;

    def download_historic_data(self, market, start, end, granularity):
        '''Prepare historic data for pretraining the model.'''
        pass;

    def take_action(self, action, type="maker"):
        '''
        Take an action.
        This method should:
            1. take an action: 0 buy, 1 hold or 2 sell.
            2. wait for some time
            3. retrieve historic_rates
            4. return

        @return: [
                  next_state,   # Vector
                  reward,       # profit compare with last time step. Float
                  done,         # if the game (trading) is over. (Set to True after one day.) Boolean.
                  info          # diagnostic information for debugging. Dict
                 ]
        '''
        pass;

    def query_price(self, market):
        '''query the price of the given market/product. For example: USDT/BTC.
        @return price: float'''
        pass;

    def buy_market(self, market, quantity, rate, timeout = None):
        pass;

    def sell_market(self, market, quantity, rate, timeout = None):
        pass;

    def cancel_order(self):
        pass;

    def query_order_book(self):
        pass

    def buy_limit(self):
        pass

    def sell_limit(self):
        pass

    def sell_stop(self):
        pass

    def conditional_order(self):
        pass
