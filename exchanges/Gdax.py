import gdax
import configparser
import time
from datetime import date, datetime, timedelta, timezone
import numpy as np
import logging
from math import ceil
from random import random
import json
import pdb

from .AbstractExchange import Exchange


class GDAX(Exchange):
    def __init__(self):
        '''Gdax Python API doc: https://github.com/danpaquin/gdax-python
        Most of the function output formats are undocumented. Please refer to
        https://docs.gdax.com/ for expected output content. We have to figure
        out output data format by ourselves.
        '''
        # logging settings
        logging.basicConfig(filename='./logs/Gdax.log', filemode='w', level=logging.DEBUG)
        logging.basicConfig(format='%(asctime)s | %(message)s')
        self.logger = logging.getLogger('GDAX CryptoQ.')
        self.logger.setLevel(logging.DEBUG)

        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        console.setFormatter(logging.Formatter('%(asctime)s | %(levelname)s | %(message)s'))
        self.logger.addHandler(console)

        console = logging.StreamHandler()
        console.setLevel(logging.ERROR)
        console.setFormatter(logging.Formatter('%(asctime)s | %(levelname)s | %(message)s'))
        self.logger.addHandler(console)

        console = logging.StreamHandler()
        console.setLevel(logging.WARNING)
        console.setFormatter(logging.Formatter('%(asctime)s | %(levelname)s | %(message)s'))
        self.logger.addHandler(console)

        # config settings
        self.apiKey         = None;
        self.apiSecret      = None;
        self.apiPassphrase  = None;
        self.market         = None;
        self.buy_pct        = 0.0; # buy with this % of USD balance
        self.periods        = 50;  # number of periods to put in state
        self.granularity    = 300; # candle duration in seconds.
        self.read_config('./config'); # read configuration data;

        # balance related fields
        self.query_client = None;
        self.auth_client  = None;
        self.asset   = 0.0; # the current number of crypto coins we have in the account
        self.capital = 0.0; # the current amount of money in USD remain in the balance
        self.init_total_usd = 0.0;      # the initial total worth of the balance in USD
        self.current_total_usd = 0.0;   # the current worth of the total balance in USD
                                        #    = self.capital
                                        #      + self.asset * asset_unit_price
        self.last_trade_total  = 0.0;
        self.prev_total_usd = None      # For computing rewards
        self._get_clients();

        # training data related fields
        self.simulation_on       = True; # True for training. False for trading.
        self.sim_data            = None;
        self.training_data_paths = [];   # list of files containing training data.
                                         # each file contains data for one day.
        self.current_data_path   = ""; # next data file path
        self.current_data_idx    = 0; # the line/row idx of data on the training
                                         #      data file.
        self.state_size          = 22 #self.periods + 2;


        self.ma5                 = None;
        self.ma10                = None;
        self.ema12               = None;
        self.ema26               = None;

    def reset(self, episode):
        if self.simulation_on:
            self.current_data_path = self.training_data_paths[episode];
            self.current_data_idx  = 0;
            self.asset   = 0.0;
            self.init_total_usd = 1000.0
            self.capital = self.init_total_usd
            self._read_historic_data(self.current_data_path)
            self.prev_total_usd = self._get_total_worth();
            self.last_trade_total = self._get_total_worth()
            # reset log file
            # TODO: Create a new log file for each episode.
        else:
            self._read_balance()
            time.sleep(0.4)
            price = self._get_asset_price();
            self.init_total_usd = self.capital + self.asset * price;
            self.prev_total_usd = self._get_total_worth();

        return self.get_state();
        pass;

    def turn_simulation(self, mode):
        '''@param mode: 'ON' or 'OFF' '''
        if mode.strip() == 'ON':
            self.simulation_on = True;
            self.capital = 1000.0;
            self.asset   = 0;
            self._get_training_files()
        elif mode.strip() == 'OFF':
            self.simulation_on = False;
            self._read_balance();

    def get_state(self):
        '''State: a list of N + 2 numbers containing:
            1. the percentage change of asset price in the last N periods
            2. current total cash / initial total worth
            3. current total asset worth / initial total worth
           where N = self.periods
        '''
        if self.simulation_on:
            # 0. The first 10 are price chagnes
            state = [];
            time_idx = self.current_data_idx
            for i in range(0, 20):
                #change = (self.sim_data[time_idx-9+i][4] - self.sim_data[time_idx-9+i-1][4])
                #change_pct = change / self.sim_data[time_idx-10+i-1][4]
                state.append(self.sim_data[time_idx-19+i][4])

            # 1. the next 10 are MA5/MA10, next 10 are EMA12 / EMA26
            '''
            for i in range(0, 5):
                state.append( self.ma5[time_idx-4 + i] )
            for i in range(0, 5):
                state.append( self.ma10[time_idx-4 + i] )
            for i in range(0, 5):
                state.append( self.ema12[time_idx-4 + i] )
            for i in range(0, 5):
                state.append( self.ema26[time_idx-4 + i] )
            '''

            state = np.array(state)
            state = state / np.linalg.norm(state)
            # 2. compute the last two elements of the state vector
            price = self._get_asset_price();
            current_total = self._get_total_worth()
            init_trade_money  = self.init_total_usd * self.buy_pct
            init_remain_money = self.init_total_usd * (1 - self.buy_pct)
            #state = np.append( state, [(self.capital-init_remain_money) /init_trade_money ], axis=0 );
            state = np.append( state, [self.asset * price / init_trade_money], axis=0);
            state = np.append( state, [(current_total - self.last_trade_total) / self.last_trade_total], axis=0);

            #print self.sim_data[self.current_data_idx][-1]
            return state;
        else:
            # 1. read market data
            price    = self._get_asset_price()
            end      = datetime.now()
            start    = end - timedelta(seconds = (self.periods+1) * self.granularity)
            response = self.query_client.get_product_historic_rates(self.market,
                                                #start = start.isoformat(),
                                                #end   = end.isoformat(),
                                                granularity = self.granularity)

            response.reverse();
            response[-1][4] = price;

            state    = [];

            # 2. compute the first n elements of the state vector
            prev_line = None;
            for idx, line in enumerate(response):
                # each line contains [time, low, high, open, close, volume]
                if idx > 0:
                    state.append( (line[4]-prev_line[4])/prev_line[4] * 100.0 )
                prev_line = line

            state = state[ : self.periods]
            # 3. compute the last two elements of the state vector
            init_trade_money = self.init_total_usd * self.buy_pct
            init_remain_money = self.init_total_usd * (1 - self.buy_pct)
            state.append( (self.capital-init_remain_money) / init_trade_money )
            state.append(self.asset * price / init_trade_money)
            state = np.array(state)
            state = state / np.linalg.norm(state)
            return state
        pass;

    def _get_asset_price(self):
        '''Get the current price of the asset'''
        if self.simulation_on:
            return self.sim_data[self.current_data_idx][4]
        else:
            response = self.query_client.get_product_ticker(product_id = self.market)
            return float(response['price'])

    def read_config(self, path):
        self.logger.info("Read config file " + path )
        config         = configparser.ConfigParser()
        config.read(path)
        self.apiKey         = config['GDAX_API']['API_KEY']
        self.apiSecret      = config['GDAX_API']['SECRET']
        self.apiPassphrase  = config['GDAX_API']['PASSPHRASE']
        self.market         = config['GDAX_API']['MARKET']
        self.buy_pct        = float(config['GDAX_API']['BUY_PCT'])/100.0
        self.periods        = int(config['GDAX_API']['PERIODS'])
        self.granularity    = int(config['GDAX_API']['GRANULARITY'])
        return;

    def _get_clients(self):
        try:
            self.query_client = gdax.PublicClient()
            self.auth_client  = gdax.AuthenticatedClient(self.apiKey,
                                                         self.apiSecret,
                                                         self.apiPassphrase)
        except Exception as e:
            self.logger.warning("Failed to get clients: ", e.message)
        return

    def _read_balance(self):
        '''read balance of the account'''
        accounts = self.auth_client.get_accounts()
        currency = self.market.split('-')[0];
        for acc in accounts:
            if acc["currency"] == 'USD':
                self.capital = float(acc["balance"])
            elif acc["currency"] == currency:
                self.asset = float(acc["balance"])
        pass;

    def _get_total_worth(self, price = None):
        if self.simulation_on:
            price = self.sim_data[self.current_data_idx][4]
        else:
            if price is None:
                price = self._get_asset_price()
        return self.asset * price + self.capital;

    def take_action(self, action, order_type="limit"):
        '''take action'''
        act_options = ["BUY ", "SELL"]
        act_str = act_options[action]

        if self.simulation_on:
            price = self._get_asset_price()
            slippage = (random()-0.5) / 200
            cmsn_rate = 0.0025;         # commission rate.
            did_trade = False
            prev_asset = self.asset;
            prev_capital = self.capital;
            if action == 0 and self.asset < 0.0001: # BUY
                buy_amount    = self.capital * self.buy_pct / (price * (1+slippage))
                self.asset   += buy_amount;
                self.capital -= buy_amount * (price * (1+slippage))
                #self.capital -= buy_amount * (price * (1+slippage)) * cmsn_rate
                did_trade = True
            elif action == 1 and self.asset > 0.0001: # SELL
                sell_amount = self.asset * (price * (1+slippage))
                self.capital += sell_amount
                self.asset   -= self.asset;
                #self.capital -= sell_amount * cmsn_rate;
                did_trade = True

            current_total   = self.asset * price + self.capital
            prev_price =self.sim_data[self.current_data_idx-1][4]
            extra = (current_total - self.prev_total_usd)/self.prev_total_usd -  (price - prev_price) / prev_price
            rewards = (current_total - self.prev_total_usd) / (self.init_total_usd * self.buy_pct) * 10000.0# + extra * 25000.0
            #print("{0} | {1} | {2} | {3}".format(current_total, self.prev_total_usd, rewards, self._get_total_worth()) )
            #if current_total == self.prev_total_usd:
            #    rewards -= 2;

            self.prev_total_usd = current_total
            if did_trade: self.last_trade_total = current_total
            self.current_data_idx += 1;
            ns      = self.get_state();
            done    = self.current_data_idx+1 == self.sim_data.shape[0] or self._get_total_worth() <= self.init_total_usd*0.8
            info    = None;

            self.logger.debug("{0}\t| Price: {1:05.2f} | Asset : {2:.8f} | USD : {3:08.2f} | Total : {4:.2f}".format(act_str, price, self.asset, self.capital, self._get_total_worth() ))
            return ns, rewards, done, info
        else:
            price = self._get_asset_price()
            time.sleep(0.1)
            order = None;
            if action == 0: # BUY
                # TODO: place only LIMIT orders when we are buying.
                if self.asset < 0.0001: # if we have no asset in hand, we can buy something
                    buy_amount    = self.capital * self.buy_pct / price
                    buy_amount    = round(buy_amount, 4)
                    # if order_type == "limit":
                    order = self.auth_client.buy(price = str(price), # USD
                                         size  = str(buy_amount), # crypto
                                         product_id = self.market,
                                         post_only  = True);
                    # else:
                    #     order = self.auth_client.buy(size  = str(buy_amount), # crypto
                    #                          type = "market",
                    #                          product_id = self.market);
                    self.logger.info("LIVE: {0} | Price: {1:05.2f}".format(act_str, price));
            else: # SELL
                if self.asset > 0.0001:
                    if order_type == "limit":
                        order = self.auth_client.sell(price = str(price), # USD
                                             size  = str(self.asset), # crypto
                                             product_id = self.market,
                                             post_only  = False);
                    else:
                        sell_amount = round(self.asset, 4)
                        order = self.auth_client.sell(size  = str(self.asset), # crypto
                                             type = "market",
                                             product_id = self.market);
                    self.logger.info("LIVE: {0} | Price: {1:05.2f}".format(act_str, price));


            order_time = datetime.now();
            while True and order:
                time.sleep(0.4)
                response = self.auth_client.get_order(order["id"])
                if response["settled"]:
                    self.logger.info("{0}".format(json.dumps(order, indent=3)))
                    break;
                if (datetime.now() - order_time).total_seconds() >= self.granularity * 0.8:
                    self.auth_client.cancel_order(order["id"]);
                    self.logger.warning("ORDER CANCELLED!!!!!")
                    break;

            time.sleep(0.4)
            self._read_balance();
            time.sleep(0.4)
            current_price = self._get_asset_price()
            current_total = self.asset * current_price + self.capital
            rewards = (current_total - self.prev_total_usd)/ (self.init_total_usd * self.buy_pct) * 10000.0
            #print("{0} | {1} | {2} | {3}".format(current_total, self.prev_total_usd, rewards, self._get_total_worth()) )
            self.prev_total_usd = current_total
            ns      = None # self.get_state();
            now     = datetime.now()
            done    = self._get_total_worth() <= self.init_total_usd*0.8 or ( now.hour == 23 and now.minute >= 55 )
            info    = None;

            self.logger.info("LIVE: {0} | Price: {1:05.2f} | Asset : {2:.8f} | USD : {3:08.2f} | Total : {4:.2f}".format(act_str, price, self.asset, self.capital, self._get_total_worth(current_price) ))
            return ns, rewards, done, info


    def download_historic_data(self, market, start, end, granularity=None):
        '''Download historic data, and store data for one day in one file.
        Each line of data contains 60 elements meaning the the percentage
        change of asset price in the last 60 periods.

        @param market: BTC-USD
        @param start: start time in ISO 8601
        @param end: end time in ISO 8601 (https://docs.python.org/2/library/datetime.html#datetime.datetime.isoformat)
        @param granularity: Desired timeslice in seconds. Can only be one of these
                numbers: {60, 300, 900, 3600}

        Each row of the save data is a vector of N+1 elements, where N = self.periods.
        The first N elements is the percentage change of close price at the end
        of every granularity seconds. The last element is the close price.

        Save by calling numpy.save():
            https://docs.scipy.org/doc/numpy/reference/generated/numpy.save.html
        '''
        if granularity is None:
            granularity = self.granularity;

        if not isinstance(start, datetime):
            self.logger.warning("start type is not datetime")
            return;
        if not isinstance(end, datetime):
            self.logger.warning("end type is not datetime")
            return;
        if not granularity in set([60, 300, 900, 3600]):
            self.logger.warning("invalid granularity")
            return;

        delta = end - start;
        for i in range(delta.days + 1):
            current_start = start + timedelta(days = i) - timedelta(seconds = self.periods * granularity);
            current_data  = (start + timedelta(days = i)).date();
            outfile = open("./training_data/" + current_data.isoformat() + ".data", 'wb')
            data = []

            self.logger.info("Downloading data on {}.".format( current_data.isoformat()))
            try:
                prev_response = None; # for handling special cases
                for hour_dlt in range(1, 25+ceil(self.periods * granularity/3600)):
                    time.sleep(0.5)

                    current_end   = current_start + timedelta(hours = 1);
                    response = self.query_client.get_product_historic_rates(market,
                                                        start = current_start.isoformat(),
                                                        end   = current_end.isoformat(),
                                                        granularity = granularity)

                    response.reverse();

                    if isinstance(response, dict):
                        self.logger.warning(response);
                        return;
                    if not len(response) == 3600 / granularity:
                        response = prev_response;
                    else:
                        prev_response = response;

                    data += response
                    current_start = current_end;

                '''
                # each line contains [time, low, high, open, close, volume]
                # we only consider the close value here
                close_changes = [];
                prev_line = None;
                for idx, line in enumerate(data):
                    if idx == 0:
                        close_changes.append(0.0);
                    else:
                        close_changes.append( (line[4] - prev_line[4]) / prev_line[4] * 100.0 )
                    prev_line = line;
                states = []; # [...changes... close price]
                length = int(3600 / granularity * 24);
                for i in range(0, length):
                    states.append( close_changes[i : i+self.periods] + [data[i+self.periods][4]] )
                states = np.array(states)
                '''
                data = np.array(data)

                self.logger.info("{} lines of data saved. ".format(data.shape[0]))
                np.save(outfile, data);
            except Exception as e:
                self.logger.warning("Failed to download data: {0}".format(e))
        pass;

    def _read_historic_data(self, path):
        '''Read historic data from file.

        Read by calling numpy.load():
            https://docs.scipy.org/doc/numpy/reference/generated/numpy.save.html
        '''
        try:
            #self.logger.debug("Read data file: " + path);
            self.sim_data        = np.load(open(path, 'rb'));
            self.current_data_path = path;
            self.current_data_idx  = 30;

            # 1. Compute MA5 and MA10
            ma5  = [self.sim_data[0][4]];
            ma10 = [self.sim_data[0][4]];
            for i in range(1, self.sim_data.shape[0]):
                ma5.append( sum(self.sim_data[i-min(i,5) : i, 4]) / min(i,5) );
            for i in range(1, self.sim_data.shape[0]):
                ma10.append( sum(self.sim_data[i-min(i,10) : i, 4]) / min(i,10) );
            self.ma5  = np.array(ma5);
            self.ma10 = np.array(ma10);

            # 2. Compute EMA12 and EMA 26
            ema12 = [ self.sim_data[0][4]*(2.0/13) ];
            ema26 = [ self.sim_data[0][4]*(2.0/27) ];
            for i in range(1, self.sim_data.shape[0]):
                ma12 = sum(self.sim_data[i-min(i, 12) : i, 4]) / min(i, 12)
                multiplier = 2.0/(12+1);
                ema12.append( (self.sim_data[i, 4] - ema12[-1]) * multiplier + ema12[-1] )

            for i in range(1, self.sim_data.shape[0]):
                ma12 = sum(self.sim_data[i-min(i, 26) : i, 4]) / min(i, 26)
                multiplier = 2.0/(26+1);
                ema26.append( (self.sim_data[i, 4] - ema26[-1]) * multiplier + ema26[-1] )

            self.ema12 = np.array( ema12 )
            self.ema26 = np.array( ema26 )

        except Exception as e:
            self.logger.warning("Failed to read data: ", e)

    def _get_training_files(self):
        '''Get stored training files'''
        from os import listdir
        from os.path import isfile, join

        dirc = './training_data/'
        paths = [dirc + f for f in listdir(dirc) if isfile(join(dirc, f))]
        #print("Simulation data files: ")
        good_paths = [];
        for path in paths:
            try:
                np.load(open(path, 'rb'));
                good_paths.append(path)
            except:
                self.logger.error("Failed to read file " + path)

        self.training_data_paths = sorted(good_paths)

        return self.training_data_paths;

    def flush_log(self):
        self.logger
        for handler in self.logger.handlers:
            handler.flush();
        return;
